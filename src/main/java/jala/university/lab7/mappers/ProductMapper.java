package jala.university.lab7.mappers;

import jala.university.lab7.infra.database.models.mongo.ProductCollection;
import jala.university.lab7.infra.database.models.postgres.Product;
import jala.university.lab7.http.dtos.request.ProductRequestDTO;
import jala.university.lab7.http.dtos.response.ProductResponseDTO;

public class ProductMapper {
    public static ProductCollection toEntity(ProductRequestDTO dto) {
        return new ProductCollection(dto.label(), dto.price(), dto.stock());
    }

    public static ProductResponseDTO toResponseDTO(ProductCollection product) {
        return new ProductResponseDTO(product.getId(), product.getLabel(), product.getPrice(), product.getStock(), product.getCreatedAt());
    }
}
