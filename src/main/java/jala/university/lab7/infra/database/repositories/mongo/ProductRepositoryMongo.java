package jala.university.lab7.infra.database.repositories.mongo;

import jala.university.lab7.infra.database.models.mongo.ProductCollection;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepositoryMongo extends MongoRepository<ProductCollection, String> {
}
