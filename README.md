# Data Migration

Você é um desenvolvedor Java que trabalha em uma empresa de comércio eletrônico. A empresa decidiu migrar seu sistema legado de armazenamento de dados de um banco de dados relacional PostgreSQL para um banco de dados NoSQL MongoDB, visando melhorar o desempenho e a escalabilidade do sistema.

## Objetivo

Desenvolver uma aplicação Spring Boot que facilite essa migração de dados do PostgreSQL para o MongoDB. Além disso, você é responsável por criar uma base de dados no PostgreSQL para realização de teste.

> Nota 1: O caso de testes é o mesmo do lab passado, com algumas modificações para atender aos requisitos desse.

> Nota 2: Os repositório JPA foram subistituídos pelos repositórios do MongoDB, a fim de atender aos requisitos da migração de banco de dados. Agora todos os dados serão inseridos no Mongo.

> Nota 3: Para vizualizamos o processo, ao iniciar a aplicação, o sistema insere automaticamente dados no Postgres para simular a migração de dados existentes.

## Testes

A configuração da migração foi feita usando alguns métodos:

1. Criação um `docker-compose` que sobe os dois serviços de banco de dados, postgres e mongodb.
2. Criação de um script usando o `CommandLineRunner` do spring, que é executado assim que a aplicação inicia.

O script se encontra em `src/main/java/jala/university/lab7/infra/database`, mas posso mostrar seus detalhes aqui:

É feito a instância dos repositórios criados para cada banco de dados e logo após, as funções de migração são chamadas, elas só executam as migrações a quais foram remetidas.

![Migration script](assets/migration-script.jpeg)

Para fins estudo, já havia um script criado que é responsável por povoar o banco de dados posgres, assim não precisamos nos preocupar em inserir dados previamente. Podemos ver nos logs da aplicação que, se houver dados no bando de dados, ele não faz nada, mas se não houver, ele insere alguns itens na tabela de produtos. De quebra, o script cria um usuário admin, capaz de realizar qualquer request no sistema, e caso ele já exista, ele não é criado novamente.

Após ligar a aplicação, podemos ver o seguinte resultado:

- Os produtos inseridos no banco de dados:
  ![Products collection](assets/products-collection.jpeg)

- O usuário Admin inserido na collection users:
  ![Products collection](assets/users-collection.jpeg)
